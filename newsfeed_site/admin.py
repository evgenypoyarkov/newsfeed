from django.contrib import admin
from models import Article, Country

# Register your models here.

# Inlines

class InlineArticle(admin.TabularInline):
  model = Article.target_countries.through

class ArticleAdmin(admin.ModelAdmin):
  prepopulated_fields = {
    'slug': ('title',)
  }
  
  list_display = (
    'title',
    'created'
  )

class CountryAdmin(admin.ModelAdmin):
  list_display = (
    'shorten',
    'title'
  )

admin.site.register(Article, ArticleAdmin)
admin.site.register(Country, CountryAdmin)
