from django.db import models

class Country(models.Model):

  # Fields
  #
  shorten = models.CharField(max_length=3, unique=True)
  title = models.CharField(max_length=20)

  # Methods
  #
  def __unicode__(self):
    return u'{} "{}"'.format(self.title, self.shorten)


  def save(self, *args, **kwargs):
    self.shorten = self.shorten.upper()
    self.title = self.title.title()

    super(Country, self).save(*args, **kwargs)


  @property
  def articles(self):
    return self.article_set.all()

class Article(models.Model):

  # Fields
  #
  slug = models.SlugField()
  title = models.CharField(max_length=80)
  body = models.TextField()
  image = models.URLField()
  created = models.DateTimeField(auto_now_add=True)

  # Relations
  #
  target_countries = models.ManyToManyField(Country)

  # Methods
  #
  def __unicode__(self):
    return self.title


  def save(self, *args, **kwargs):
    self.title = self.title.title()

    super(Article, self).save(*args, **kwargs)
