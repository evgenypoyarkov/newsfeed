from django.views.generic import DetailView
from django.views.generic.list import ListView

from newsfeed_site.models import Country, Article
# Create your views here.

class MainPageView(ListView):
  template_name = 'newsfeed_site/index.html'

  model = Country

  def get_context_data(self, **kwargs):
    context = super(MainPageView, self).get_context_data(**kwargs)

    requestedCountry = self.request.GET.get('country', '').upper()

    if not requestedCountry:
      requestedCountry = self.request.session.get('lastObservedCountry')

    try:
      context['current'] = context['country_list'].get(shorten=requestedCountry)
      context['article_list'] = context['current'].articles
      self.request.session['lastObservedCountry'] = requestedCountry
    except Country.DoesNotExist:
      context['article_list'] = []

    return context

class ArticlePageView(DetailView):
  template_name = 'newsfeed_site/article.html'

  model = Article

  def get_context_data(self, **kwargs):
    context = super(ArticlePageView, self).get_context_data(**kwargs)
    return context
