from django.conf.urls import patterns, url

from newsfeed_site.views import MainPageView, ArticlePageView

urlpatterns = patterns('',
  url(r'^$', MainPageView.as_view()),
  url(r'^articles/(?P<slug>[-_\w]+)/$', ArticlePageView.as_view()),
)
